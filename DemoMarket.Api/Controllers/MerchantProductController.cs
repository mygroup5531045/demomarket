﻿using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseCommands;
using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseQuerys;
using DemoMarket.Application.UseCases.Products.ProductCaseQuerys;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoMarket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MerchantProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetProduchtAsync([FromQuery] GetMerchantProductQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProductAsync([FromQuery] GetAllMerchantProductQuery query)
        {
            var responce = await _mediator.Send(query);

            return Ok(responce);
        }

        /// <summary>
        /// They add a merchant product to the base
        /// </summary>
        /// <param name="command"></param>
        [HttpPost]
        public async Task<IActionResult> CreateProductAsync([FromForm] CreateMerchantProductCommand command)
        {
            var responce = await _mediator.Send(command);

            return Ok(responce);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateProductAsync([FromForm] UpdateMerchantProductCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProductAsync([FromForm] DeleteMerchantProductCommand command)
        {
            var responce = await _mediator.Send(command);

            return Ok(responce);
        }
    }
}
