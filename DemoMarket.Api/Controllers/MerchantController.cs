﻿using DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands;
using DemoMarket.Application.UseCases.Merchants.MerchantCaseQueris;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoMarket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MerchantController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MerchantController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// They add a merchant to the base
        /// </summary>
        /// <param name="command"></param>
        [HttpPost]
        public async Task<IActionResult> CreateMerchantAsync([FromForm] CreateMerchantCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetMerchantAsync([FromQuery] GetMerchantQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllMerchantAsync([FromQuery] GetAllMerchantQuery query)
        {
            var responce = await _mediator.Send(query);

            return Ok(responce);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateMerchantAsync([FromForm] UpdateMerchantCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteMerchantAsync([FromForm] DeleteMerchantCommand command)
        {
            var responce = await _mediator.Send(command);

            return Ok(responce);
        }
    }
}
