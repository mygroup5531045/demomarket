﻿using DemoMarket.Application.UseCases.Products.ProductCaseCommands;
using DemoMarket.Application.UseCases.Products.ProductCaseQuerys;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoMarket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetProduchtAsync([FromQuery] GetProductQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProductAsync([FromQuery] GetAllProductQuery query)
        {
            var responce = await _mediator.Send(query);

            return Ok(responce);
        }

        /// <summary>
        /// They add a product product to the base
        /// </summary>
        /// <param name="command"></param>
        [HttpPost]
        public async Task<IActionResult> CreateProductAsync([FromForm] CreateProductCommand command)
        {
            var responce = await _mediator.Send(command);

            return Ok(responce);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateProductAsync([FromForm] UpdateProductCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProductAsync(DeleteProductCommand command)
        {
            var responce = await _mediator.Send(command);

            return Ok(responce);
        }
    }
}
