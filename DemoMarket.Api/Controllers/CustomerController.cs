﻿using DemoMarket.Application.UseCases.Customers.CustomerCaseCommands;
using DemoMarket.Application.UseCases.Customers.CustomerCaseQuerys;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoMarket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetAdminAsync([FromQuery] GetCustomerQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAdminAsync([FromQuery] GetAllCustomerQuery query)
        {
            var response = await _mediator.Send(query);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAdminAsync([FromForm] CreateCustomerCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAdminAsync([FromForm] UpdateCustomerCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAdminAsync([FromForm] DeleteCustomerCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }
    }
}
