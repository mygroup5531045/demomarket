﻿using DemoMarket.Application.UseCases.AdminAuth.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoMarket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AuthController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("Login")]
  
        public async Task<IActionResult> LoginAsAdmin(LoginAdminCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }
    }
}
