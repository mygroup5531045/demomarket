﻿using DemoMarket.Application.UseCases.Admins.AdminCaseCommands;
using DemoMarket.Application.UseCases.Admins.AdminCaseQuerys;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DemoMarket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IMediator  _mediator;

        public AdminController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAdminAsync(CreateAdminCommand command)
        {
           var response = await _mediator.Send(command);

            return Ok(response);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAdminAsync([FromForm] DeleteAdminCommand command)
        {
          var responce = await _mediator.Send(command);

            return Ok(responce);
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetAdminAsync([FromQuery] GetAdminQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetAllAdminAsync([FromQuery] GetAllAdminQuery query)
        {
            var response = await _mediator.Send(query);

            return Ok(response);
        }

        [HttpGet("id")]
        public async Task<IActionResult> UpateAdminAsync([FromQuery] UpdateAdminCommand command)
        {
            var response = await _mediator.Send(command);

            return Ok(response);
        }
    }
}
