﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.Abstractions
{
    public interface IApplicationDbContext 
    {
        DbSet<Admin> Admins { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<Instalment> Instalments { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<InstalmentDebt> InstalmentsDebts { get; set; }
        DbSet<Manufacturer> Manufacturers { get; set; } 
        DbSet<Merchant> Merchant { get; set; }
        DbSet<MerchantProduct> MerchantProducts { get; set; }
        DbSet<OrderDetail> OrderDetails { get; set; }
        DbSet<Purchase> Purchases { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    }
}
