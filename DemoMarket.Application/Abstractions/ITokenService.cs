﻿using DemoMarket.Domain.Entities;

namespace DemoMarket.Application.Abstractions
{
    public interface ITokenService
    {
        string GenerateAccessToken(Admin admin);
    }
}
