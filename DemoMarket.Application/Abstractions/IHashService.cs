﻿namespace DemoMarket.Application.Abstractions
{
    public interface IHashService
    {
        string GetHash(string value);
    }
}
