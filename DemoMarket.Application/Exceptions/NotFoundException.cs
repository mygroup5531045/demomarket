﻿namespace DemoMarket.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        private const string _message = "Not Found";
        public NotFoundException()
           : base(_message)
        {
        }
    }
}
