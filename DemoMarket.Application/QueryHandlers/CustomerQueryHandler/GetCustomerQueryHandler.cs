﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Customers.CustomerCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.CustomerQueryHandler
{
    public class GetCustomerQueryHandler : IQueryHandler<GetCustomerQuery, CustomerViewQuery>
    {
        private readonly IApplicationDbContext _context;

        public GetCustomerQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<CustomerViewQuery> Handle(GetCustomerQuery request, CancellationToken cancellationToken)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (customer == null)
            {
                throw new NotFoundException();
            }

            return new CustomerViewQuery()
            {
                Id = customer.Id,
                Name =customer.Name,
                Phone=customer.Phone,
                UserName=customer.UserName,
                Balance =customer.Balanse,
            };
        }
    }
}
