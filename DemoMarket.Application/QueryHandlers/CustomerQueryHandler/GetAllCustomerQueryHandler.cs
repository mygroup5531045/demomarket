﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Customers.CustomerCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.CustomerQueryHandler
{
    public class GetAllCustomerQueryHandler : IQueryHandler<GetAllCustomerQuery, List<CustomerViewQuery>>
    {
        private readonly IApplicationDbContext _context;

        public GetAllCustomerQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<CustomerViewQuery>> Handle(GetAllCustomerQuery request, CancellationToken cancellationToken)
        {
            var customers = await _context.Customers.ToListAsync(cancellationToken);

            if (customers == null)
            {
                throw new NotFoundException();
            }

            var customerList = new List<CustomerViewQuery>();

            foreach(var customer in customers)
            {
                var newCustomer = new CustomerViewQuery()
                {
                    Balance = customer.Balanse,
                    Id = customer.Id,
                    Name = customer.Name,
                    Phone = customer.Phone,
                    UserName = customer.UserName
                };
                customerList.Add(newCustomer);
            }
            return customerList;
        }
    }
}
