﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Admins.AdminCaseQuerys;

namespace DemoMarket.Application.QueryHandlers.AdminQueryHandler
{
    public class GetAllAdminQueryHandler : IQueryHandler<GetAllAdminQuery, List<AdminViewQuery>>
    {
        private readonly IApplicationDbContext _context;

        public GetAllAdminQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<AdminViewQuery>> Handle(GetAllAdminQuery query, CancellationToken cancellationToken)
        {
            var admins = _context.Admins.ToList();

            if (admins == null)
            {
                throw new NotFoundException();
            }
            var newAdmins = new List<AdminViewQuery>();

            foreach (var admin in admins)
            {
                var newAdmin = new AdminViewQuery()
                {
                    Email = admin.Email,
                    Phone = admin.Phone,
                    Id = admin.Id,
                    FullName = admin.Name,
                    UserName = admin.UserName
                };

                newAdmins.Add(newAdmin);
            }
            return newAdmins;
        }
    }
}
