﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Admins.AdminCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.AdminQueryHandler
{
    public class GetAdminQueryHandler : IQueryHandler<GetAdminQuery, AdminViewQuery>
    {
        private readonly IApplicationDbContext _context;

        public GetAdminQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<AdminViewQuery> Handle(GetAdminQuery query, CancellationToken cancellationToken)
        {
            var admin = await _context.Admins.FirstOrDefaultAsync(x => x.Id == query.Id, cancellationToken);

            if (admin == null)
            {
                throw new NotFoundException();
            }

            return   new AdminViewQuery()
                   {
                       Email = admin.Email,
                       Phone = admin.Phone,
                       Id = admin.Id,
                       FullName = admin.Name,
                       UserName = admin.UserName,
                   };
        }
    }
}
