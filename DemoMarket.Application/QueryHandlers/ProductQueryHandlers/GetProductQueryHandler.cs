﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies.ProductModels;
using DemoMarket.Application.UseCases.Products.ProductCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.ProductQueryHandlers
{
    public class GetProductQueryHandler : IQueryHandler<GetProductQuery, ProductViewQuery>
    {
        private readonly IApplicationDbContext _context;

        public GetProductQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<ProductViewQuery> Handle(GetProductQuery query, CancellationToken cancellationToken)
        {
            var product = await _context.Products
                             .Include(c => c.Category)
                             .Include(m => m.Manufacturer)
                             .FirstOrDefaultAsync(x => x.Id == query.Id);

            return new ProductViewQuery()
            {
                Name = product!.Name,
                CategoryDescription = product.Category.Description,
                CategoryName = product.Category.Name,
                ManufacturerCountry = product.Manufacturer.Country,
                ManufacturerName = product.Manufacturer.Name
            };
        }
    }
}
