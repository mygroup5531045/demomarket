﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies.ProductModels;
using DemoMarket.Application.UseCases.Products.ProductCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.ProductQueryHandlers
{
    public class GetAllProductQueryHandler : IQueryHandler<GetAllProductQuery, List<ProductViewQuery>>
    {
        private readonly IApplicationDbContext _context;

        public GetAllProductQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<ProductViewQuery>> Handle(GetAllProductQuery query, CancellationToken cancellationToken)
        {
            var products =await _context.Products
                                 .Include(c=>c.Category)
                                 .Include(m=>m.Manufacturer)
                                 .ToListAsync();

            var productsList = new List<ProductViewQuery>();

            foreach (var product in products)
            {
                var newProduct = new ProductViewQuery()
                {
                    Name = product.Name,
                    CategoryName = product.Category.Name,
                    ManufacturerName = product.Manufacturer.Name,
                    CategoryDescription = product.Category.Description,
                    ManufacturerCountry = product.Manufacturer.Country,
                };
                productsList.Add(newProduct);
            }

            return productsList;
        }
    }
}
