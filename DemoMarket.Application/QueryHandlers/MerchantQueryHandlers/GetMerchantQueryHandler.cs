﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Merchants.MerchantCaseQueris;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.MerchantQueryHandlers
{
    public class GetMerchantQueryHandler : IQueryHandler<GetMerchantQuery, MerchantViewQuery>
    {
        private readonly IApplicationDbContext _context;

        public GetMerchantQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<MerchantViewQuery> Handle(GetMerchantQuery request, CancellationToken cancellationToken)
        {
            var merchant = await _context.Merchant.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (merchant == null)
            {
                throw new NotFoundException();
            }

            return new MerchantViewQuery()
            {
                Id = merchant.Id,
                Name =  merchant.Name,
                Balanse = merchant.Balance,
                Phone = merchant.Phone,
                UserName = merchant.UserName,
            };
        }
    }
}
