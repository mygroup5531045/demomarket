﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Merchants.MerchantCaseQueris;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.MerchantQueryHandlers
{
    public class GetAllMerchantQueryHandler : IQueryHandler<GetAllMerchantQuery, List<MerchantViewQuery>>
    {
        private readonly IApplicationDbContext _context;

        public GetAllMerchantQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<MerchantViewQuery>> Handle(GetAllMerchantQuery request, CancellationToken cancellationToken)
        {
            var merchants = await _context.Merchant.ToListAsync();

            if (merchants == null)
            {
                throw new NotFoundException();
            }
            var newMerchants = new List<MerchantViewQuery>();

            foreach (var merchant in merchants)
            {
                var newMerchant = new MerchantViewQuery()
                {
                    Phone = merchant.Phone,
                    Id = merchant.Id,
                    Name = merchant.Name,
                    UserName = merchant.UserName,
                    Balanse = merchant.Balance,
                };

                newMerchants.Add(newMerchant);
            }

            return newMerchants;
        }
    }
}
