﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.MerchantProductQueryHandlers
{
    public class GetAllMerchantProductQueryHandler : IQueryHandler<GetAllMerchantProductQuery, List<MerchantProductViewQuery>>
    {
        private readonly IApplicationDbContext _context;

        public GetAllMerchantProductQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<MerchantProductViewQuery>> Handle(GetAllMerchantProductQuery request, CancellationToken cancellationToken)
        {
            var merchantProducts = await _context.MerchantProducts
                                                .Include(p => p.Product)
                                                .ThenInclude(c => c.Category)
                                                .Include(p => p.Product)
                                                .ThenInclude(m => m.Manufacturer)
                                                .Include(m => m.Merchant)
                                                .ToListAsync();

            if (request == null)
            {
                throw new NotFoundException();
            }

            var newMerchantProduct =new List<MerchantProductViewQuery>();

            foreach (var merchantProduct in merchantProducts)
            {
               var merchant =  new MerchantProductViewQuery()
                {
                    Id = merchantProduct.Id,
                    Count = merchantProduct.Count,
                    Price = merchantProduct.Price,
                    MerchantName = merchantProduct.Merchant.Name!,
                    MerchantBalance = merchantProduct.Merchant.Balance,
                    ProductCatigoriyName = merchantProduct.Product.Category.Name!,
                    ProductManufacturerName = merchantProduct.Product.Manufacturer.Name!,
                    ProductManufacturerCountry = merchantProduct.Product.Manufacturer.Country!,
                    ProductCatigoriyDescription = merchantProduct.Product.Category.Description!,
                };

                newMerchantProduct.Add(merchant);
            }

            return newMerchantProduct;
        }
    }
}
