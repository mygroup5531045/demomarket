﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseQuerys;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.QueryHandlers.MerchantProductQueryHandlers
{
    public class GetMerchantProductQueryHandler : IQueryHandler<GetMerchantProductQuery, MerchantProductViewQuery>
    {
        private readonly IApplicationDbContext _context;

        public GetMerchantProductQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<MerchantProductViewQuery> Handle(GetMerchantProductQuery request, CancellationToken cancellationToken)
        {
            var merchentProduct =await _context.MerchantProducts
                                   .Include(p=>p.Product)
                                   .ThenInclude(c=>c.Category)
                                   .Include(p=>p.Product)
                                   .ThenInclude(m=>m.Manufacturer)
                                   .Include(m=>m.Merchant)
                                   .FirstOrDefaultAsync(r=>r.Id == request.Id);

            if (request == null) 
            {
                throw new NotFoundException();
            }

            return new MerchantProductViewQuery()
            {
                Id = merchentProduct!.Id,
                Count = merchentProduct.Count,
                Price = merchentProduct.Price,
                MerchantName = merchentProduct.Merchant.Name!,
                MerchantBalance = merchentProduct.Merchant.Balance,
                ProductCatigoriyName = merchentProduct.Product.Category.Name!,
                ProductManufacturerName = merchentProduct.Product.Manufacturer.Name!,
                ProductManufacturerCountry = merchentProduct.Product.Manufacturer.Country!,
                ProductCatigoriyDescription = merchentProduct.Product.Category.Description!,
            };
        }
    }
}
