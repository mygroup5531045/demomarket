﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies;

namespace DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseQuerys
{
    public class GetMerchantProductQuery : IQuery<MerchantProductViewQuery>
    {
        public int Id { get; set; }
    }
}
