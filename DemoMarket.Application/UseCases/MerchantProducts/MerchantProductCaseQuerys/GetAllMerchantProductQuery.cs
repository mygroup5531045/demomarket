﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies;

namespace DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseQuerys
{
    public class GetAllMerchantProductQuery : IQuery<List<MerchantProductViewQuery>>
    {
    }
}
