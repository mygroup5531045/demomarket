﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseCommands
{
    public class DeleteMerchantProductCommand : ICommand<int>
    {
        public int Id { get; set; }
    }
}
