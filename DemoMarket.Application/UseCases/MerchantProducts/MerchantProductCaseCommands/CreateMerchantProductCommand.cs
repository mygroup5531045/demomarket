﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseCommands
{
    public class CreateMerchantProductCommand : ICommand<int>
    {
        public double Price { get; set; }

        public int Count { get; set; }

        public int ProductId { get; set; }

        public int MerchantId { get; set; }
    }
}
