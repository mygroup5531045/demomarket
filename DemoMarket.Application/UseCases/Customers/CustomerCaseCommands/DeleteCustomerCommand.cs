﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.Customers.CustomerCaseCommands
{
    public class DeleteCustomerCommand :ICommand<int>
    {
        public int Id { get; set; }
    }
}
