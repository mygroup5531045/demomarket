﻿using DemoMarket.Application.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace DemoMarket.Application.UseCases.Customers.CustomerCaseCommands
{
    public class CreateCustomerCommand : ICommand<CreateCustomerCommand>
    {
        [Required]
        public string? UserName { get; set; }
        [Required]
        public string? Password { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }
        [Required]
        public double Balance { get; set; }
    }
}
