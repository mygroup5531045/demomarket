﻿using DemoMarket.Application.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace DemoMarket.Application.UseCases.Customers.CustomerCaseCommands
{
    public class UpdateCustomerCommand :ICommand<UpdateCustomerCommand>
    {
        public int Id { get; set; }

        [Required]
        public string? UserName { get; set; }
        [Required]
        public string? Password { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }
        [Required]
        public double Balanse { get; set; }
    }
}
