﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies;

namespace DemoMarket.Application.UseCases.Customers.CustomerCaseQuerys
{
    public class GetCustomerQuery : IQuery<CustomerViewQuery>
    {
        public int Id { get; set; }
    }
}
