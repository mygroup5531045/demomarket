﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies;

namespace DemoMarket.Application.UseCases.Merchants.MerchantCaseQueris
{
    public class GetAllMerchantQuery : IQuery<List<MerchantViewQuery>>
    {
    }
}
