﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies;
using System.ComponentModel.DataAnnotations;

namespace DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands
{
    public class UpdateMerchantCommand : ICommand<MerchantViewQuery>
    {
        public int Id { get; set; }

        [Required]
        public string? UserName { get; set; }

        [Required]
        public string? Password { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }
        [Required]
        public double Balance { get; set; }
    }
}
