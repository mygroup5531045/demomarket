﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands
{
    public class DeleteMerchantCommand : ICommand<int>
    {
        public int Id { get; set; }
    }
}
