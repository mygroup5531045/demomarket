﻿using DemoMarket.Application.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands
{
    public class CreateMerchantCommand : ICommand<CreateMerchantCommand>
    {
        [Required]
        public string? UserName { get; set; }
        [Required]
        public string? Password { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }
        [Required]
        public double Balance { get; set; }
    }
}
