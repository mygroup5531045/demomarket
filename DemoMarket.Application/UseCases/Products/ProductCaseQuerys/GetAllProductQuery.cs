﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies.ProductModels;

namespace DemoMarket.Application.UseCases.Products.ProductCaseQuerys
{
    public class GetAllProductQuery : IQuery<List<ProductViewQuery>>
    {
    }
}
