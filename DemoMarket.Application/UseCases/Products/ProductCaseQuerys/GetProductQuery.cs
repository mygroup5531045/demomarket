﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies.ProductModels;

namespace DemoMarket.Application.UseCases.Products.ProductCaseQuerys
{
    public class GetProductQuery :IQuery<ProductViewQuery>
    {
        public int Id { get; set; }
    }
}
