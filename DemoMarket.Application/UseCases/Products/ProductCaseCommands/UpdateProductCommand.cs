﻿using DemoMarket.Application.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace DemoMarket.Application.UseCases.Products.ProductCaseCommands
{
    public class UpdateProductCommand : ICommand<int>
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public int ManufacturerId { get; set; }
    }
}
