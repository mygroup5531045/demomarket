﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.Products.ProductCaseCommands
{
    public class DeleteProductCommand : ICommand<int>
    {
        public int Id { get; set; }
    }
}
