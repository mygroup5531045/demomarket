﻿using DemoMarket.Application.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace DemoMarket.Application.UseCases.Products.ProductCaseCommands
{
    public class CreateProductCommand : ICommand<CreateProductCommand>
    {
        [Required]
        public string? Name { get; set; } = null;

        [Required]
        public int CategoryId { get; set; }

        public int ManufacturerId { get; set; }
    }
}
