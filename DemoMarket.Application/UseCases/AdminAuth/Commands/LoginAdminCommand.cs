﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Domain.Entities;
using DemoMarket.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.UseCases.AdminAuth.Commands
{
    public class LoginAdminCommand : ICommand<string>
    {
        public string? Username { get; set; }

        public string? Password { get; set; }

    }

    public class LoginAdminCommandHandler : ICommandHandler<LoginAdminCommand, string>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHashService _hashService;
        private readonly ITokenService _tokenService;

        public LoginAdminCommandHandler(IApplicationDbContext context, IHashService hashService, ITokenService tokenService)
        {
            _context = context;
            _hashService = hashService;
            _tokenService = tokenService;
        }

        public async Task<string> Handle(LoginAdminCommand command, CancellationToken cancellationToken)
        {
            var admin = await _context.Admins.FirstOrDefaultAsync(x=>x.UserName == command.Username,cancellationToken);

            if(admin == null)
            {
                throw new LoginException(new EntityNotFoundException(nameof(Admin)));
            }

            if(admin.PasswordHash != _hashService.GetHash(command.Password!))
            {
                throw new LoginException();
            }
            return _tokenService.GenerateAccessToken(admin);
        }
    }
}
