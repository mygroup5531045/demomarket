﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.Admins.AdminCaseCommands
{
    public class UpdateAdminCommand : ICommand<string>
    {
        public int Id { get; set; }

        public string? UserName { get; set; }

        public string? FullName { get; set; }

        public string? Password { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }
    }
}
