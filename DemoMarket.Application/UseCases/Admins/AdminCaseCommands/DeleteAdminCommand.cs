﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.Admins.AdminCaseCommands
{
    public class DeleteAdminCommand : ICommand<string>
    {
        public int Id { get; set; }
    }
}
