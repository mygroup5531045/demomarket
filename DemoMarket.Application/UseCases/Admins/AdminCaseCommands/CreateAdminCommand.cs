﻿using DemoMarket.Application.Abstractions;

namespace DemoMarket.Application.UseCases.Admins.AdminCaseCommands
{
    public class CreateAdminCommand : ICommand<CreateAdminCommand>
    {
        public string? UserName { get; set; }

        public string? Password { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }
    }
}
