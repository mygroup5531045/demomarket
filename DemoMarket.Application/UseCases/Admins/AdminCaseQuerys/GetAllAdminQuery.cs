﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Models.Queryies;

namespace DemoMarket.Application.UseCases.Admins.AdminCaseQuerys
{
    public class GetAllAdminQuery : IQuery<List<AdminViewQuery>>
    {
    }
}
