﻿namespace DemoMarket.Application.Models.Queryies
{
    public class MerchantViewQuery
    {
        public int Id { get; set; }

        public string? UserName { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }

        public double Balanse { get; set; }
    }
}
