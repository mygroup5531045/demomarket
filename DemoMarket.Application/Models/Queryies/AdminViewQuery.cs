﻿namespace DemoMarket.Application.Models.Queryies
{
    public class AdminViewQuery
    {
        public int Id { get; set; }

        public string? UserName { get; set; }

        public string? FullName { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }
    }
}
