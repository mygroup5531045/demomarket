﻿namespace DemoMarket.Application.Models.Queryies
{
    public class MerchantProductViewQuery
    {
        public int Id { get; set; }

        public double Price { get; set; }

        public int Count { get; set; }

        public string MerchantName { get; set; }

        public double MerchantBalance { get; set; }

        public string ProductCatigoriyName { get; set; }

        public string ProductCatigoriyDescription { get; set; }

        public string ProductManufacturerName { get; set; }

        public string ProductManufacturerCountry { get; set; }

    }
}
