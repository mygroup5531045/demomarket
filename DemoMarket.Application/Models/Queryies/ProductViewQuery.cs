﻿namespace DemoMarket.Application.Models.Queryies.ProductModels
{
    public class ProductViewQuery
    {
        public string? Name { get; set; }

        public string? CategoryName { get; set; }

        public string? CategoryDescription { get; set; }

        public string? ManufacturerName { get; set; }

        public string? ManufacturerCountry { get; set; }
    }
}
