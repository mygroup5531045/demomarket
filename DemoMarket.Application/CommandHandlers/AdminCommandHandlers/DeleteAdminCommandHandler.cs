﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.Admins.AdminCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.AdminCommandHandlers
{
    public class DeleteAdminCommandHandler : ICommandHandler<DeleteAdminCommand, string>
    {
        private readonly IApplicationDbContext _context;

        public DeleteAdminCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<string> Handle(DeleteAdminCommand command, CancellationToken cancellationToken)
        {
            var admin = await _context.Admins.FirstOrDefaultAsync(x => x.Id == command.Id, cancellationToken);

            if (admin == null)
            {
                throw new NotFoundException();
            }

            _context.Admins.Remove(admin);
            await _context.SaveChangesAsync();

            return admin.Id.ToString();
        }
    }
}
