﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.UseCases.Admins.AdminCaseCommands;

namespace DemoMarket.Application.CommandHandlers.Admin
{
    public class CreateAdminCommandHandler : ICommandHandler<CreateAdminCommand, CreateAdminCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHashService _hashService;

        public CreateAdminCommandHandler(IApplicationDbContext context, IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        public async Task<CreateAdminCommand> Handle(CreateAdminCommand command, CancellationToken cancellationToken)
        {
            var admin = new Domain.Entities.Admin()
            {
                Email = command.Email,
                Phone = command.Phone,
                Name = command.Name,
                UserName = command.UserName,
                PasswordHash = _hashService.GetHash(command.Password!)
            };

            await _context.Admins.AddAsync(admin, cancellationToken);
            await _context.SaveChangesAsync();

            return command;
        }
    }
}
