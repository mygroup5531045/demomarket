﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.Admins.AdminCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.AdminCommandHandlers
{
    public class UpdateAdminCommandHandler : ICommandHandler<UpdateAdminCommand, string>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHashService _hashService;

        public UpdateAdminCommandHandler(IApplicationDbContext context, IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        public async Task<string> Handle(UpdateAdminCommand command, CancellationToken cancellationToken)
        {
            var entity = await _context.Admins.FirstOrDefaultAsync(x => x.Id == command.Id, cancellationToken);

            if (command == null)
            {
                throw new NotFoundException();
            }

            entity!.Name = command.FullName ?? entity.Name;
            entity.Email = command.Email ?? entity.Email;
            entity.Phone = command.Phone ?? entity.Phone;
            entity.PasswordHash = _hashService.GetHash(command.Password!) ?? entity.PasswordHash;
            entity.UserName = command.UserName ?? entity.UserName;
           

           _context.Admins.Update(entity);
            await _context.SaveChangesAsync();

            return command.Id.ToString();
        }
    }
}
