﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.Products.ProductCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.ProductCommandHandlers
{
    public class DeleteProductCommandHandler : ICommandHandler<DeleteProductCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public DeleteProductCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(DeleteProductCommand command, CancellationToken cancellationToken)
        {
            var product = await _context.Products.FirstOrDefaultAsync(x=>x.Id == command.Id,cancellationToken);

            if (product == null) 
            {
                throw new NotFoundException();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return command.Id;
        }
    }
}
