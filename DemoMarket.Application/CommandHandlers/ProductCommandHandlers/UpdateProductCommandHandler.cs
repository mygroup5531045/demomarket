﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.Products.ProductCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.ProductCommandHandlers
{
    public class UpdateProductCommandHandler : ICommandHandler<UpdateProductCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public UpdateProductCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(UpdateProductCommand command, CancellationToken cancellationToken)
        {
            var product = await _context.Products.Include(m=>m.Manufacturer).Include(c=>c.Category).FirstOrDefaultAsync(x=>x.Id == command.Id,cancellationToken);

            if (product == null)
            {
                throw new NotFoundException();
            }

            product.Name = command.Name ?? product.Name;
            product.ManufacturerId = command.ManufacturerId;
            product.CategoryId = command.CategoryId;

            _context.Products.Update(product);
            await _context.SaveChangesAsync(cancellationToken);

            return command.Id;
        }
    }
}
