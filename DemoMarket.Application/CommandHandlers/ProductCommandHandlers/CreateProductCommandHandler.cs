﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.UseCases.Products.ProductCaseCommands;
using DemoMarket.Domain.Entities;

namespace DemoMarket.Application.CommandHandlers.ProductCommandHandlers
{
    public class CreateProductCommandHandler : ICommandHandler<CreateProductCommand, CreateProductCommand>
    {
        private readonly IApplicationDbContext _contexr;

        public CreateProductCommandHandler(IApplicationDbContext contexr)
        {
            _contexr = contexr;
        }

        public async Task<CreateProductCommand> Handle(CreateProductCommand command, CancellationToken cancellationToken)
        {
            var product = new Product()
            {
                Name = command.Name,
                CategoryId = command.CategoryId,
                ManufacturerId = command.ManufacturerId,
            };

            await _contexr.Products.AddAsync(product);
            await _contexr.SaveChangesAsync();

            return command;
        }
    }
}
