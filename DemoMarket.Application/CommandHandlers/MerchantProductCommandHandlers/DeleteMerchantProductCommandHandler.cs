﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.MerchantProductCommandHandlers
{
    public class DeleteMerchantProductCommandHandler : ICommandHandler<DeleteMerchantProductCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public DeleteMerchantProductCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(DeleteMerchantProductCommand request, CancellationToken cancellationToken)
        {
            var merchantProduct = await _context.MerchantProducts.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
           
            if (merchantProduct == null)
            {
              throw new NotFoundException();
            }

            _context.MerchantProducts.Remove(merchantProduct);
            await _context.SaveChangesAsync();

            return merchantProduct.Id;
        }
    }
}
