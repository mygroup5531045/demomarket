﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.MerchantProductCommandHandlers
{
    public class UpdataMerchantProductCommandHandler : ICommandHandler<UpdateMerchantProductCommand, UpdateMerchantProductCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdataMerchantProductCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UpdateMerchantProductCommand> Handle(UpdateMerchantProductCommand request, CancellationToken cancellationToken)
        {
            var merchantProduct = await _context.MerchantProducts.FirstOrDefaultAsync(x=>x.Id == request.Id,cancellationToken);

            if (merchantProduct == null) 
            {
                throw new NotFoundException();
            }

            merchantProduct.Count = request.Count;
            merchantProduct.MerchantId = request.MerchantId;
            merchantProduct.ProductId = request.ProductId;
            merchantProduct.Price = request.Price;

            _context.MerchantProducts.Update(merchantProduct);
            await _context.SaveChangesAsync();

            return new UpdateMerchantProductCommand()
            {
                Count = merchantProduct.Count,
                Price =merchantProduct.Price,
                Id = merchantProduct.Id,
                ProductId = merchantProduct.ProductId,
                MerchantId =merchantProduct.MerchantId
            };
        }
    }
}
