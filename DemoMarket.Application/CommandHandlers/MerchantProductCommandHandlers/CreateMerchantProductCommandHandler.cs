﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.UseCases.MerchantProducts.MerchantProductCaseCommands;
using DemoMarket.Domain.Entities;

namespace DemoMarket.Application.CommandHandlers.MerchantProductCommandHandlers
{
    internal class CreateMerchantProductCommandHandler : ICommandHandler<CreateMerchantProductCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreateMerchantProductCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateMerchantProductCommand request, CancellationToken cancellationToken)
        {
            var merchantProduct = new MerchantProduct()
            {
                Count = request.Count,
                MerchantId = request.MerchantId,
                ProductId = request.ProductId,
                Price = request.Price,
               
            };
            
            await _context.MerchantProducts.AddAsync(merchantProduct);
            await _context.SaveChangesAsync();

            return merchantProduct.Id;
        }
    }
}
