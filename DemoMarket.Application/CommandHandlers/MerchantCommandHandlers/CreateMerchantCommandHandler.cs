﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands;
using DemoMarket.Domain.Entities;

namespace DemoMarket.Application.CommandHandlers.MerchantCommandHandlers
{
    public class CreateMerchantCommandHandler : ICommandHandler<CreateMerchantCommand, CreateMerchantCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHashService _hashService;

        public CreateMerchantCommandHandler(IApplicationDbContext context,IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        public async Task<CreateMerchantCommand> Handle(CreateMerchantCommand request, CancellationToken cancellationToken)
        {
            var merchant = new Merchant()
            {
                UserName = request.UserName,
                Name = request.Name,
                Phone = request.Phone,
                Balance = request.Balance,
                PasswordHash = _hashService.GetHash(request.Password),
            };

            await _context.Merchant.AddAsync(merchant,cancellationToken);
            await _context.SaveChangesAsync();

            return request;

        }
    }
}
