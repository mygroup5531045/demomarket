﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.MerchantCommandHandlers
{
    public class DeleteMerchantCommandHandler : ICommandHandler<DeleteMerchantCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public DeleteMerchantCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(DeleteMerchantCommand request, CancellationToken cancellationToken)
        {
            var merchant = await _context.Merchant.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (merchant == null) 
            {
                throw new NotFoundException();
            }

            _context.Merchant.Remove(merchant);
            await _context.SaveChangesAsync();

            return request.Id;
        }
    }
}
