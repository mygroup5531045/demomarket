﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.Models.Queryies;
using DemoMarket.Application.UseCases.Merchants.MerchantCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.MerchantCommandHandlers
{
    public class UpdateMerchantCommandHandler : ICommandHandler<UpdateMerchantCommand, MerchantViewQuery>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHashService _hashService;

        public UpdateMerchantCommandHandler(IApplicationDbContext context,IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        public async Task<MerchantViewQuery> Handle(UpdateMerchantCommand request, CancellationToken cancellationToken)
        {
            var merchant = await _context.Merchant.FirstOrDefaultAsync(x=>x.Id == request.Id,cancellationToken);

            if (merchant == null)
            {
                throw new NotFoundException();
            }

            merchant.Name = request.Name ?? merchant.Name;
            merchant.UserName = request.UserName;
            merchant.Balance = request.Balance;
            merchant.Phone = request.Phone ?? merchant.Phone;
            merchant.PasswordHash = _hashService.GetHash(request.Password!);

            _context.Merchant.Update(merchant);
            await _context.SaveChangesAsync();

            return new MerchantViewQuery()
            {
                Id = request.Id,
                Balanse =request.Balance,
                Name =  request.Name,
                Phone = request.Phone,
                UserName = request.UserName,
            };
        }
    }
}
