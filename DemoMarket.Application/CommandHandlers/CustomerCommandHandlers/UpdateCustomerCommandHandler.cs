﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.Exceptions;
using DemoMarket.Application.UseCases.Customers.CustomerCaseCommands;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Application.CommandHandlers.CustomerCommandHandlers
{
    public class UpdateCustomerCommandHandler : ICommandHandler<UpdateCustomerCommand, UpdateCustomerCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHashService _hashService;

        public UpdateCustomerCommandHandler(IApplicationDbContext context, IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        public async Task<UpdateCustomerCommand> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            if (customer == null)
            {
                throw new NotFoundException();
            }
            customer.Phone =request.Phone;
            customer.Balanse = request.Balanse;
            customer.Name = request.Name ?? customer.Name;
            customer.UserName = request.UserName ?? customer.UserName;
            customer.PasswordHash = _hashService.GetHash(request.Password!) ?? customer.PasswordHash;
            

            _context.Customers.Update(customer);
            await _context.SaveChangesAsync();

            return new UpdateCustomerCommand()
            {
                Phone = customer.Phone,
                Balanse= customer.Balanse,
                Id = customer.Id,
                Name = customer.Name,
                UserName = customer.UserName,
                Password = request.Password
            };
        }
    }
}
