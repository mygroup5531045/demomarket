﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Application.UseCases.Customers.CustomerCaseCommands;
using DemoMarket.Domain.Entities;

namespace DemoMarket.Application.CommandHandlers.CustomerCommandHandlers
{
    public class CreateCustomerCommandHandler : ICommandHandler<CreateCustomerCommand, CreateCustomerCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateCustomerCommandHandler(IApplicationDbContext context,IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }

        private readonly IHashService _hashService;
        public async Task<CreateCustomerCommand> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = new Customer()
            {
                Balanse = request.Balance,
                Name = request.Name,
                UserName = request.UserName,
                Phone = request.Phone,
                PasswordHash = _hashService.GetHash(request.Password!)
            };

            await _context.Customers.AddAsync(customer);
            await _context.SaveChangesAsync(cancellationToken);

            return new CreateCustomerCommand()
            {
               Balance = customer.Balanse,
               Name = customer.Name,
               Password = request.Password,
               Phone = request.Phone,
               UserName =customer.UserName
            };
        }
    }
}
