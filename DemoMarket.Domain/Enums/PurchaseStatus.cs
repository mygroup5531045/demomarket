﻿namespace DemoMarket.Domain.Enums
{
    public enum PurchaseStatus
    {
        Created,
        Pending,
        Successful,
        Canceled
    }
}
