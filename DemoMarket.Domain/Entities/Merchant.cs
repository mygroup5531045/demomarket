﻿namespace DemoMarket.Domain.Entities
{
    public class Merchant : User
    {

        public Merchant()
        {
            MerchantProducts = new HashSet<MerchantProduct>();
        }

        public double Balance { get; set; }

        public ICollection<MerchantProduct> MerchantProducts { get; set; }
    }
}
