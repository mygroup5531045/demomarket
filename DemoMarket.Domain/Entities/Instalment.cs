﻿namespace DemoMarket.Domain.Entities
{
    public class Instalment
    {
        public Instalment()
        {
            instalmentDebts = new HashSet<InstalmentDebt>();
        }
        public int Id { get; set; }

        public DateTime StartData { get; set; }

        public int Months { get; set; }

        public double Persent { get; set; }

        public double InitialFree { get; set; }

        public int OrderId { get; set; }


        public Order Order { get; set; }

        public ICollection<InstalmentDebt> instalmentDebts { get; set; }
    }
}

