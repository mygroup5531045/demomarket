﻿namespace DemoMarket.Domain.Entities
{
    public class Product
    {

        public Product()
        {
            MerchantProducts = new HashSet<MerchantProduct>();
        }
        public int Id { get; set; }

        public string? Name { get; set; }

        public int CategoryId { get; set; }

        public int ManufacturerId { get; set; }


        public Category Category { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public ICollection<MerchantProduct> MerchantProducts { get; set; }
    }
}
