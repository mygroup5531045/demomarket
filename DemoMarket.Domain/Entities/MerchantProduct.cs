﻿namespace DemoMarket.Domain.Entities
{
    public class MerchantProduct
    {
        public MerchantProduct()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }
        public int Id { get; set; }

        public double Price { get; set; }

        public int Count { get; set; }

        public int ProductId { get; set; }

        public int MerchantId { get; set; }


        public Product Product { get; set; }

        public Merchant Merchant { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
