﻿namespace DemoMarket.Domain.Entities
{
    public class InstalmentDebt
    {
        public InstalmentDebt() { }

        public int Id { get; set; }

        public DateTime PaynetData { get; set; }

        public bool IsPay { get; set; }

        public int Sum { get; set; }


        public int InstalmentId { get; set; }

        public Instalment Instalment { get; set; }
    }
}
