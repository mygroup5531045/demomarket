﻿namespace DemoMarket.Domain.Entities
{
    public class Customer : User
    {
        public Customer()
        {
            Orders = new HashSet<Order>();
        }
        public double Balanse { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
