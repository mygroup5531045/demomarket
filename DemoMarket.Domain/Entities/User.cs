﻿namespace DemoMarket.Domain.Entities
{
    public class User
    {
        public int Id { get; set; }

        public string? UserName { get; set; }

        public string? PasswordHash { get; set; }

        public string? Name { get; set; }

        public string? Phone { get; set; }
    }
}
