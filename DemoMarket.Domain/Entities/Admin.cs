﻿namespace DemoMarket.Domain.Entities
{
    public class Admin : User
    {
        public string? Email { get; set; }
    }
}
