﻿namespace DemoMarket.Domain.Entities
{
    public class Order
    {

        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public double TotalSum { get; set; }

        public int CustomerId { get; set; }


        public Customer Customer { get; set; }

        public Instalment Instalment { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
