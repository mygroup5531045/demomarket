﻿namespace DemoMarket.Infrastructure.Configurations
{
    public class JwtTokenConfiguration 
    {
       public string ValidAudiense {  get; set; }

       public string ValidIssuer { get; set; }

       public string  Secret {  get; set; }
    }
}
