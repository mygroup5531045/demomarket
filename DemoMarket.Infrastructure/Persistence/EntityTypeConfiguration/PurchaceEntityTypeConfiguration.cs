﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class PurchaceEntityTypeConfiguration : IEntityTypeConfiguration<Purchase>
    {
        void IEntityTypeConfiguration<Purchase>.Configure(EntityTypeBuilder<Purchase> builder)
        {
            builder.HasKey(x => x.Id);

           builder.HasOne(p=>p.OrderDetail)
                .WithMany(p=>p.Purchases)
                .HasForeignKey(p=>p.OrderDetailId);

        }
    }
}
