﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class MerchantEntityTypeConfiguration : IEntityTypeConfiguration<Merchant>
    {
        void IEntityTypeConfiguration<Merchant>.Configure(EntityTypeBuilder<Merchant> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.UserName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(x => x.UserName)
                .IsUnicode();
        }
    }
}
