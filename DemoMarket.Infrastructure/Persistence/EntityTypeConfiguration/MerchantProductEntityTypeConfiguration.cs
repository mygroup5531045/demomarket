﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class MerchantProductEntityTypeConfiguration : IEntityTypeConfiguration<MerchantProduct>
    {
        void IEntityTypeConfiguration<MerchantProduct>.Configure(EntityTypeBuilder<MerchantProduct> builder)
        {
            builder.HasKey(x => x.Id);

           builder.HasOne(p=>p.Product)
                .WithMany(p=>p.MerchantProducts)
                .HasForeignKey(p=>p.ProductId);

            builder.HasOne(p => p.Merchant)
                 .WithMany(p => p.MerchantProducts)
                 .HasForeignKey(p => p.MerchantId);
        }
    }
}
