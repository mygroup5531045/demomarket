﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class OrderDetailEntityTypeConfiguration : IEntityTypeConfiguration<OrderDetail>
    {
        void IEntityTypeConfiguration<OrderDetail>.Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(p => p.Order)
               .WithMany(p => p.OrderDetails)
               .HasForeignKey(p => p.OrderId);

            builder.HasOne(p => p.MerchantProduct)
                 .WithMany(p => p.OrderDetails)
                 .HasForeignKey(p => p.MerchantProductId);
        }
    }
}
