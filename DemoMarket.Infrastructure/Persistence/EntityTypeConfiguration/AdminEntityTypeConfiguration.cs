﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class AdminEntityTypeConfiguration : IEntityTypeConfiguration<Admin>
    {
        void IEntityTypeConfiguration<Admin>.Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.UserName)
                .HasMaxLength(50)
                .IsRequired();

            builder.Property(x => x.UserName)
                .IsUnicode();

            builder.HasData(new User
            {
                Id = 1,
                UserName = "Admin",
                PasswordHash =
               "2646720E1B4B3B960107335AC274F819510741B41A2254C7F17FF39110C89919D0F10F29E2A5BAC9976E2CA3358B1AC65BDC63AEAB83B59F792F188EDE1C9846",
                Name = "Adminov admin",
                Phone = "998948189994",

            });
        }
    }
}
