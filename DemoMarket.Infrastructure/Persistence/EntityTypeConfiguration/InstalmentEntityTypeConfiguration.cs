﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class InstalmentEntityTypeConfiguration : IEntityTypeConfiguration<Instalment>
    {
        void IEntityTypeConfiguration<Instalment>.Configure(EntityTypeBuilder<Instalment> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Order)
                 .WithOne(x => x.Instalment)
                 .HasForeignKey<Instalment>(x => x.OrderId);
        }
    }
}
