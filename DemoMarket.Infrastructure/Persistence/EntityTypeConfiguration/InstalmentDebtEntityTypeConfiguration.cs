﻿using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Infrastructure.Persistence.EntityTypeConfiguration
{
    public class InstalmentDebtEntityTypeConfiguration : IEntityTypeConfiguration<InstalmentDebt>
    {
        void IEntityTypeConfiguration<InstalmentDebt>.Configure(EntityTypeBuilder<InstalmentDebt> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(p => p.Instalment)
                 .WithMany(p => p.instalmentDebts)
                 .HasForeignKey(p => p.InstalmentId);
        }
    }
}
