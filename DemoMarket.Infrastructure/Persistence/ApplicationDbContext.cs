﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Instalment> Instalments { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<InstalmentDebt> InstalmentsDebts { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Merchant> Merchant { get; set; }
        public DbSet<MerchantProduct> MerchantProducts { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Purchase> Purchases { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }
    }
}
