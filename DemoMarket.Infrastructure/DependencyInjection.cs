﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Domain.Entities;
using DemoMarket.Infrastructure.Abstractuons;
using DemoMarket.Infrastructure.Persistence;
using DemoMarket.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace DemoMarket.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
           
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddScoped<IApplicationDbContext,ApplicationDbContext>();
            services.AddScoped<IHashService, HashService>();
            services.AddScoped<ITokenService, JwtService>();
            services.AddScoped<IAuthService, AuthService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
              {
                  options.TokenValidationParameters = new TokenValidationParameters
                  {
                      ValidateIssuer = true,
                      ValidateIssuerSigningKey = true,
                      ValidateLifetime = true,
                      ValidateAudience = true,
                      ValidIssuer = configuration["JWT:ValidIssuer"],
                      ValidAudience = configuration["JWT:validAudiense"],
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
                  };
              });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminActions", polisy =>
                {
                    polisy.RequireClaim("Role", nameof(Admin));
                });
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("MerchantAction", polisy =>
                {
                    polisy.RequireClaim("Role", nameof(Merchant));
                });
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("CustomerAction", polisy =>
                {
                    polisy.RequireClaim("Role", nameof(Customer));
                });
            });

            return services;
        }
    }
}