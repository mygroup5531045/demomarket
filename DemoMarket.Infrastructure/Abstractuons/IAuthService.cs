﻿namespace DemoMarket.Infrastructure.Abstractuons
{
    public interface IAuthService
    {
        Task<string> LoginAsync(string username, string password);
    }
}
