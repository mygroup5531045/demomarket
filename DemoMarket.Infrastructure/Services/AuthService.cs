﻿using DemoMarket.Application.Abstractions;
using DemoMarket.Infrastructure.Abstractuons;
using DemoMarket.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace DemoMarket.Infrastructure.Services
{
    public class AuthService : IAuthService
    {
        private readonly ApplicationDbContext _dbcontext;
        private readonly ITokenService _tokenService;
        private readonly IHashService _hashService;

        public AuthService(ApplicationDbContext dbContext, ITokenService tokenService, IHashService hashProvider)
        {
            _dbcontext = dbContext;
            _tokenService = tokenService;
            _hashService = hashProvider;
        }

        public async Task<string> LoginAsync(string userName, string password)
        {
            var user = await _dbcontext.Admins.FirstOrDefaultAsync(x => x.UserName == userName);

            if (user == null)
            {
                throw new Exception("User Not found");
            }
            else if (user.PasswordHash != _hashService.GetHash(password))
            {
                throw new Exception("Password is wrong");
            }
            return _tokenService.GenerateAccessToken(user);
        }
    }
}
